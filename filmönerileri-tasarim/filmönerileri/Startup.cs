﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(filmonerileri.Startup))]
namespace filmonerileri
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
