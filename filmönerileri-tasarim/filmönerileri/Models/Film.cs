﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace filmönerileri.Models
{
    public class Film
    {
        public int FilmID { get; set; }
        public string FilmAdi { get; set; }
        public string Filmİcerigi { get; set; }
        public string Kategori { get; set; }
        public byte[] FilmResim { get; set; }

    }
}