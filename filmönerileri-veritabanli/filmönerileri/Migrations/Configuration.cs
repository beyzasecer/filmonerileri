namespace filmönerileri.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<filmonerileri.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "filmonerileri.Models.ApplicationDbContext";
        }

        protected override void Seed(filmonerileri.Models.ApplicationDbContext context)
        {


            context.Categories.AddOrUpdate(c=> c.Name,
                new Category { Name = "Horror" },
                new Category { Name = "Action" },
                new Category { Name = "Romance" },
                new Category { Name = "Sci-Fi" },
                new Category { Name = "Drama" },
                new Category { Name = "Comedy" }
            );
            context.SaveChanges();


        }
    }
}
