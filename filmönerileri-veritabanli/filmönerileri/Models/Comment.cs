﻿using filmonerileri.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace filmönerileri.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public int FilmId { get; set; }
        public Film Film { get; set; }
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }
        public string Body { get; set; }
    }
}