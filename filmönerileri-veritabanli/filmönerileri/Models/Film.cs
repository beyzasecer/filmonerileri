﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace filmönerileri.Models
{
    public class Film
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(20, ErrorMessage ="Film adı 100 karakterden daha uzun olamaz.")]
        public string Name { get; set; }
        public int Rating { get; set; }
        public int ReleaseYear { get; set; }
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        public Category Category{ get; set; }
        [ForeignKey("Category")]
        public int CategoryId { get; set; }
        [DataType(DataType.Upload)]
        public byte[] Image { get; set; }
    }
}