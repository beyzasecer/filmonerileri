﻿using filmonerileri.Models;
using filmönerileri.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace filmönerileri.Controllers
{
    public class CommentsController : Controller
    {
        ApplicationDbContext context = new ApplicationDbContext();
        [ChildActionOnly]
        public ActionResult Index([Bind(Prefix ="id")] int FilmId)
        {
            var model = context.Comments.Include("User").Where(c => c.Film.Id == FilmId);
            return PartialView("_Comments", model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult NewComment(NewCommentViewModel comment, [Bind(Prefix = "id")] int FilmId)
        {
            if (ModelState.IsValid)
            {
                string userId = User.Identity.GetUserId();
                context.Comments.Add(new Comment
                {
                    Body = comment.Body,
                    FilmId = FilmId,
                    UserId = userId
                });
                context.SaveChanges();                
            }
            return RedirectToAction("Details", "Films", new { Id = FilmId });

        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if(context != null)
            {
                context.Dispose();
            }
        }

    }
}