﻿using filmonerileri.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace filmönerileri.Controllers
{
    public class FilmsController : Controller
    {
        ApplicationDbContext context = new ApplicationDbContext();
        public ActionResult Index(string category)
        {
            var model = context.Films.Where(f => f.Category.Name == category);
            return View(model);
        }

        public ActionResult Details(int id)
        {
            var model = context.Films.Find(id);
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if(context != null)
            {
                context.Dispose();
            }
        }
    }
}