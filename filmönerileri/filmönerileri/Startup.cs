﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(filmönerileri.Startup))]
namespace filmönerileri
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
