﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace filmönerileri.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            /*ViewBag.Message = "Your application description page.";*/

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Bizimle iletişime geçin";

            return View();
        }
        
        public ActionResult Korku()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Aksiyon()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Romantik()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Bilimkurgu()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Dram()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Komedi()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}