﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace filmönerileri.Models
{
    public class NewMoviewViewModel
    {
        public string Name { get; set; }
        public int Rating { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public int ReleaseYear { get; set; }
    }
}