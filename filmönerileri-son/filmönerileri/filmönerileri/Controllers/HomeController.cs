﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace filmonerileri.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            /*ViewBag.Message = "Your application description page.";*/

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Bizimle iletişime geçin";

            return View();
        }
        
        public ActionResult Korku()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Aksiyon()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Romantik()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Bilimkurgu()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Dram()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Komedi()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult ChangeLanguage(string lang, string returnUrl)
        {
            if (lang != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(lang);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(lang);
            }
            HttpCookie cookie = new HttpCookie("Language");
            cookie.Value = lang;
            Response.Cookies.Add(cookie);

            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");

        }
    }
}